// Back to front string
function backToFront(string, count) {
	if (count <= 0 || count > 10) {
		return string;
	}

	const suffix = string.slice(-count);
	return suffix + string + suffix;
}

console.log(backToFront("hello", 1));
console.log(backToFront("abc", 3));
console.log(backToFront("world", 2));
console.log(backToFront("world", 20));

// Nearest number
function nearest(z, x, y) {
	const diffX = Math.abs(z - x);
	const diffY = Math.abs(z - y);

	if (diffX <= diffY) {
		return x;
	} else {
		return y;
	}
}

console.log(nearest(100, 22, 122));
console.log(nearest(50, 22, 122));

//Remove array duplicates
function removeDuplicate(arr) {
	var uniqueArr = [];
	for (var i = 0; i < arr.length; i++) {
		if (uniqueArr.indexOf(arr[i]) === -1) {
			uniqueArr.push(arr[i]);
		}
	}
	return uniqueArr;
}

console.log(removeDuplicate([1, 2, 3, 2, 3, 1, 1]));
console.log(removeDuplicate(["a", 1, "2", "b", 1, "2", "b"]));
